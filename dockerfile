# base image mit maven/eclipse/jdk17
#FROM maven:3.9-eclipse-temurin-17-alpine

# Kopiere die beiden Codequellen
#COPY pom.xml .
#COPY src src

# maven build prozess
#RUN mvn package

# Container Start-Befehl
#ENTRYPOINT ["java -DDB_USERNAME="jokedbuser" -DDB_PASSWORD="123456" -jar target/architecture-refcard-03-0.0.1-SNAPSHOT.jar"]

# First stage: Build the application
FROM maven:3.8.6-openjdk-11-slim AS build

# Set the working directory
WORKDIR /app

# Copy the pom.xml and source code
COPY pom.xml .
COPY src ./src

# Package the application
RUN mvn package

# Second stage: Create the runtime image
FROM openjdk:11-jre-slim

# Set the working directory
WORKDIR /app

# Copy the JAR file from the build stage
COPY --from=build /app/target/architecture-refcard-03-0.0.1-SNAPSHOT.jar /app/app.jar

# Environment variables for database credentials
ENV DB_USERNAME=jokedbuser
ENV DB_PASSWORD=123456
ENV DB_URL=jdbc:mariadb://database:3306/jokedb

# Expose the application port
EXPOSE 8080

# Run the application
ENTRYPOINT ["java", "-jar", "/app/app.jar"]

